# Overview

I recently installed a Nest v3 thermostat in my condo and had a tough time finding good documentation for people in my place, so I documented what I did in hopes others will find it helpful.

## Old thermostat

The thermostat I replaced was a Honeywell TB8575A1000.(**NOTE** A black wire was connected to the R terminal before I took the photo.)

![alt text](images/honeywell.jpg)

## Nest Thermostat

I installed the Nest Learning Thermostat v3.

![alt text](images/nest.jpg)

## Caveats

If you experience any errors within the Nest app after connecting the wiring, navigate to the Pro Install settings and ensure the fields are set to match the following diagram. (Use the ring on the Nest to scroll through the wiring)

![alt text](images/wiring.png)

## Disclaimer

I claim no responsibility if your installation does not work after following these steps.
